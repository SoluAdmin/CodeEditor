var gulp = require('gulp');
var notify = require('gulp-notify');
var codecept = require('gulp-codeception');
var _ = require('lodash');

var codeceptionOptions = {
    debug: false,
    clear: true,
    notify: true,
    flags: "--colors "
};

gulp.task('runtest', function() {
    gulp.src('codeception.yml')
        .pipe(codecept('./vendor/bin/codecept', codeceptionOptions))
        .on('error', notify.onError(testNotification('fail', 'codeception')))
        .pipe(notify(testNotification('pass', 'codeception')));
});

function testNotification(status, pluginName, override) {
    console.log(status);
    var options = {
        title: (status === 'pass') ? 'Tests Passed' : 'Tests Failed',
        message: (status === 'pass') ? '\n\nAll tests have passed!\n\n' : '\n\nOne or more tests failed...\n\n' + status,
        icon: __dirname + '/node_modules/gulp-' + pluginName + '/assets/test-' + status + '.png'
    };
    options = _.merge(options, override);
    return options;
}

gulp.task('watch', function () {
    gulp.watch(['tests/**/*.php', 'src/**/*.php'], ['runtest']);
});

gulp.task('default', ['runtest', 'watch']);