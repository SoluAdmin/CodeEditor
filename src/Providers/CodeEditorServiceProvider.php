<?php

namespace SoluAdmin\CodeEditor\Providers;

use Backpack\CRUD\CrudServiceProvider as CRUDRouter;
use SoluAdmin\Support\Providers\CrudServiceProvider;
use Illuminate\Routing\Router;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class CodeEditorServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
    ];

    protected $resources = [
        'file' => 'FileCrudController'
    ];

    protected function registerExtras()
    {
        $this->setupExtraRoutes($this->app->router);
    }

    protected function bootExtras()
    {
        $this->publishes([__DIR__ . '/../../resources/extras/views' => base_path('resources/views')], 'views');
    }

    private function setupExtraRoutes(Router $router)
    {
        $router->group(
            ['namespace' => "SoluAdmin\\CodeEditor\\Http\\Controllers"],
            function () use ($router) {
                $router->group([
                    'prefix' => config('backpack.base.route_prefix', 'admin')
                        . config('SoluAdmin.CodeEditor.route_prefix', ''),
                    'middleware' => config("SoluAdmin.CodeEditor.middleware")
                        ? array_merge(['web', 'admin'], config("SoluAdmin.CodeEditor.middleware"))
                        : ['web', 'admin'],
                ], function () use ($router) {
                    CRUDRouter::resource('file', 'FileCrudController')->with(
                        function () use ($router) {
                            $router->get('file/{file}/edit-code', 'FileCrudController@editCode')
                                ->name('file.edit-code');
                            $router->post('file/{file}/edit-code', 'FileCrudController@updateCode')
                                ->name('file.edit-code');
                        }
                    );
                });
            }
        );
    }
}
