<?php

namespace SoluAdmin\CodeEditor\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('SoluAdmin.CodeEditor.tables_prefix') . 'files');
    }
}
