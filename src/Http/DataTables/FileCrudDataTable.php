<?php

namespace SoluAdmin\CodeEditor\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class FileCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::CodeEditor.name'),
            ],
            [
                'name' => 'path',
                'label' => trans('SoluAdmin::CodeEditor.path'),
            ],
        ];
    }
}
