<?php

namespace SoluAdmin\CodeEditor\Http\Forms;

use SoluAdmin\CodeEditor\Models\File;
use SoluAdmin\Support\Interfaces\Form;

class FileCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::CodeEditor.name'),
            ],
            [
                'name' => 'path',
                'label' => trans('SoluAdmin::CodeEditor.path'),
                'type' => 'select_from_array',
                'options' => [
                    app_path('PageTemplates') => trans('SoluAdmin::CodeEditor.paths.page-templates'),
                    resource_path('lang/es/tenants') => trans('SoluAdmin::CodeEditor.paths.es-translations'),
                    resource_path('lang/en/tenants') => trans('SoluAdmin::CodeEditor.paths.en-translations')
                ],
                'allows_null' => false,
            ],
        ];
    }

    public function editCodeFields(File $file)
    {
        $fileContent = file_get_contents("{$file->path}/{$file->name}.php");

        return [
            [
                'name' => 'code',
                'label' => trans('SoluAdmin::CodeEditor.code'),
                'type' => 'ace',
                'value' => $fileContent,
            ],
        ];
    }
}
