<?php

namespace SoluAdmin\CodeEditor\Http\Controllers;

use Prologue\Alerts\Facades\Alert;
use Illuminate\Http\Request;
use SoluAdmin\CodeEditor\Http\Requests\FileCrudRequest as StoreRequest;
use SoluAdmin\CodeEditor\Models\File;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class FileCrudController extends BaseCrudController
{
    public function setup()
    {
        parent::setup();
        $this->crud->denyAccess(['edit', 'update']);
        $this->crud->addButtonFromView('line', 'edit_code', 'edit_code', 'edit_code');
    }

    public function store(StoreRequest $request)
    {
        $path = $request->get('path');
        $type = basename($path);
        $name = $request->get('name');

        $fileContent = view("stubs.code-editor.{$type}", compact('name'))->render();
        file_put_contents("{$path}/{$name}.php", $fileContent);
        return parent::storeCrud();
    }

    public function destroy($id)
    {
        $file = File::findOrFail($id);

        $destroyResult = parent::destroy($id);

        if ($destroyResult) {
            unlink("{$file->path}/{$file->name}.php");
        }

        return $destroyResult;
    }

    public function editCode($id)
    {
        $file = File::findOrFail($id);

        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->form()->editCodeFields($file);
        $this->data['title'] = trans('SoluAdmin::CodeEditor.edit_code') . ' ' . $this->crud->entity_name;
        $this->data['entry'] = $file;

        return view('SoluAdmin::CodeEditor.edit_code', $this->data);
    }

    public function updateCode($id, Request $request)
    {
        $file = File::findOrFail($id);

        file_put_contents("{$file->path}/{$file->name}.php", $request->get('code'));

        Alert::success(trans('SoluAdmin::CodeEditor.code_changed', ['file' => $file->name]))->flash();

        return redirect()->to($this->crud->getRoute());
    }
}
