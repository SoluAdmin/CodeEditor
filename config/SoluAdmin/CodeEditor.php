<?php

return [
    'route_prefix' => '/code-editor',
    'middleware' => false, // If we need to secure it we use middlewares, ex: ['can:do-anything']
    'setup_routes' => false,
    'publishes_migrations' => true,
    'tables_prefix' => 'code_editor_',
];
