@if(\Auth::user()->hasModule('SoluAdmin\\CodeEditor'))
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.CodeEditor.route_prefix', '') . '/file') }}">
        <i class="fa fa-code"></i> <span>{{trans('SoluAdmin::CodeEditor.module_name')}}</span>
    </a>
</li>
@endif