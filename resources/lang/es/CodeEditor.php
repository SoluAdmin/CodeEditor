<?php

return [
    'module_name' => 'Editor de código',
    'file_singular' => 'Archivo',
    'file_plural' => 'Archivos',
    'name' => 'Nombre de archivo',
    'path' => 'Ruta',
    'code' => 'Código',
    'edit_code' => 'Editar código',
    'save_code' => 'Guardar cambios',
    'paths' => [
        'page-templates' => 'Plantillas de páginas',
        'es-translations' => 'Traduciones en español',
        'en-translations' => 'Traduciones en inglés',
    ],
    'code_changed' => 'Códifo del archivo :file cambiado',
];
