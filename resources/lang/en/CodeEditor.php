<?php

return [
    'module_name' => 'Code Editor',
    'file_singular' => 'File',
    'file_plural' => 'Files',
    'name' => 'Filename',
    'path' => 'Path',
    'code' => 'Code',
    'edit_code' => 'Edit code',
    'save_code' => 'Save changes',
    'paths' => [
        'page-templates' => 'Page Templates',
        'es-translations' => 'Spanish Translations',
        'en-translations' => 'English Translations',
    ],
    'code_changed' => ':file code changed',
];
