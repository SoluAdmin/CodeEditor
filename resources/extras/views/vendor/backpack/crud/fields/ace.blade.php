<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <div
        id="ace-{{ $field['name'] }}"
        @include('crud::inc.field_attributes', ['default_class' => 'form-control ace-editor'])
        >{{isset($field['value']) ? $field['value'] : '' }}</div>
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <style type="text/css">
        .ace-editor {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        </style>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <script src="/vendor/ace-editor/ace.js"></script>
    @endpush

@endif

@push('crud_fields_scripts')
<script>
    var editor = ace.edit("ace-{{ $field['name'] }}");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/php");
</script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
