<a href="{{ url($crud->route.'/'.$entry->getKey(). '/edit-code') }}"
   class="btn btn-xs btn-default">
    <i class="fa fa-code"></i> {{ trans('SoluAdmin::CodeEditor.edit_code') }}
</a>
